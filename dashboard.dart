import 'package:developement/splash.dart';
import 'package:flutter/material.dart';


 void main()  => runApp(
  MaterialApp(
    home: Dashboard(),
    debugShowCheckedModeBanner: false,
  )
  ); 
  
  class Dashboard  extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: new AppBar(
        title: Center(child:Text("DASHBOARD",style: TextStyle(fontWeight: FontWeight.bold , color: Colors.white),) ) ,
         backgroundColor: Colors.blue, 
      ),
        backgroundColor: Colors.white,
        floatingActionButton: FloatingActionButton(onPressed: (){
          Navigator.push(context, new MaterialPageRoute(builder: (context) => Home()
          )); 
        }
        ),
      

        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children:<Widget> [



              Padding(padding: EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment : MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(Icons.menu, color: Colors.black,size: 140.0, ),
                  Text("Menu",style: TextStyle(color: Colors.black,fontSize: 30),),
                  Icon(Icons.person_pin_circle,size: 140.0, ),
                  Text("Profile",style: TextStyle(color: Colors.black,fontSize: 30 ))
                  ,
                ],
               crossAxisAlignment: CrossAxisAlignment.center
              ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                 ),
                 Padding(
                   padding: EdgeInsets.all(20.1),
                   child: Center(
                    child: Wrap(
                      children:<Widget> [
                        SizedBox(
                          width: 200.0,
                          height: 200.0,
                          child: Card(
                            color: Colors.grey,
                        child : Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Column(children: <Widget> [
                            Icon(Icons.payment_outlined, 
                            color: Colors.black, size : 100.0),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text("PAYMENT METHOD",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize:18.0),)
                          ]
                          ),
                          )
                          ),
                          ),
                          SizedBox(
                          width: 200.0,
                          height: 200.0,
                          child: Card(
                            color: Colors.grey,
                        child : Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Column(children: <Widget> [
                            Icon(Icons.emoji_transportation_sharp,color: Colors.black, size : 100.0),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text("AVAILABLE DRIVERS",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize:18.0),)
                          ]
                          ),
                          )
                          ),
                          ),
                          SizedBox(
                          width: 200.0,
                          height: 200.0,
                          child: Card(
                            color: Colors.grey,
                        child : Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Column(children: <Widget> [
                            Icon(Icons.history,color: Colors.black, size : 100.0),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text("HISTORY",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize:18.0),)
                          ]
                          ),
                          )
                          ),
                          ),
                          SizedBox(
                          width: 200.0,
                          height: 200.0,
                          child: Card(
                            color: Colors.grey,
                        child : Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Column(children: <Widget> [
                            Icon(Icons.settings,color: Colors.black, size : 100.0),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text("SETTINGS",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize:18.0),)
                          ]
                          ),
                          )
                          ),
                          ),
                      ],
                    )
                    ),
                    ),
            ],
            )
            ),
        
      );
    }
  }
  

  


