import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     title: "NOCTURNAL",
     theme: ThemeData(
      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: Colors.pink).copyWith(
          secondary:
           Colors.brown),
     ),
     home : const SplashScreen(),
     debugShowCheckedModeBanner: false,
     
    );
  }
}
class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      
      splash: Column(
        children: [
          Image.asset('assets/girl.png'),
          const Text('RIDING APP', style: TextStyle(fontWeight: FontWeight.bold,fontSize:20,color: Colors.white),
    )]),
        backgroundColor: Colors.black,
       nextScreen: const Home(),
       splashIconSize: 300,
       duration: 5000,
       splashTransition: SplashTransition.rotationTransition,
       );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    appBar:  AppBar(
      title: const Text("SIGN IN"),
      centerTitle: true,
    ),
 body: 

 Padding ( 
  padding: EdgeInsets.all(25),
 child : Center( 
    child: Form(
    child: Column(
    children: <Widget> [

     Icon(Icons.person,color: Colors.grey, size : 150,),
      TextFormField(
    decoration: InputDecoration(
      border:OutlineInputBorder(), labelText: "Email/Username"),    
  ),

  Padding(padding: EdgeInsets.only(
    top: 20.0,
  ),
  child: TextFormField(
decoration: InputDecoration(
  border: OutlineInputBorder(), labelText: "Password" ),
  ),
  ),

  Padding(padding: EdgeInsets.only(
    top: 20.0),
  child: RaisedButton(onPressed: () {},
  child: Text("Log In") ,),
  ),

Padding(padding: EdgeInsets.all(8.0),
child : Center (
child: Row(
  children: <Widget> [
  Text("Do not have an account yet?", style: TextStyle(color: Colors.black,fontSize:12 ),)
],)
)),

  Padding(padding: EdgeInsets.only(
    top: 20.0),
  child: RaisedButton(onPressed: () {},
  child: Text("Register") ,) ,
  )
  ]
  ,)
  )
   ,),
)

    );
  }
}